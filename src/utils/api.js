import artistsJson from "../MusicArtists"

export const updateData = (data) => localStorage.setItem('artists', JSON.stringify(data));
export const getData = () => {
    let res = JSON.parse(localStorage.getItem('artists'));
    if (!res){
        res = artistsJson.artist_list.map(artist => {
            return {...artist.artist, Comments:""}
        });
        updateData(res);
    }
    return res;
};

export const editItem = (obj) => {
    const data = getData();
    const result = data.map((item) => {
        if (item.artist_id === obj.artist_id) {
            return {
                ...obj,
            };
        }
        return item;
    });
    updateData(result);
};
