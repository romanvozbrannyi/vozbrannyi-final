import React, {Component} from 'react';

class Details extends Component {
    render() {
        const {selectedArtist} = this.props;
        return (
            <div id="exampleModal" className="modal fade" id="exampleModal" tabIndex="-1" role="dialog"
                 aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <form onSubmit={this.props.handleSubmit}>
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">

                                <div className="form-group">
                                    <label>Id</label>
                                    <input value={selectedArtist.artist_id} type="text" className="form-control" readOnly/>
                                </div>
                                <div className="form-group">
                                    <label>Name</label>
                                    <input required maxLength="50" onChange={this.props.handleChange} value={selectedArtist.artist_name} name="artist_name" type="text" className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <label>Country</label>
                                    <input value={selectedArtist.artist_country} type="text" className="form-control" readOnly/>
                                </div>
                                <div className="form-group">
                                    <label>Rating</label>
                                    <input required min="0" max="100" onChange={this.props.handleChange} value={selectedArtist.artist_rating} name="artist_rating" type="number" className="form-control"/>
                                </div>
                                <div className="form-group">
                                    <label>Update Time</label>
                                    <input value={selectedArtist.updated_time} type="text" className="form-control" readOnly/>
                                </div>
                                <div className="form-group">
                                    <label>Twitter URL</label>
                                    <input value={selectedArtist.artist_twitter_url} type="text" className="form-control" readOnly/>
                                </div>
                                <div className="form-group">
                                    <label>Comments</label>
                                    <textarea
                                        onChange={this.props.handleChange}
                                        className="form-control"
                                        value={selectedArtist.Comments}
                                        name="Comments"
                                        rows="2"
                                    />
                                </div>

                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.props.handleModal}>Close</button>
                                <button type="submit" className="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Details;
