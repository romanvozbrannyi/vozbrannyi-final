import React, {Component} from 'react';
import '../App.css';
import {getData, editItem} from '../utils/api';
import Table from "./Table";
import Search from "./Search";
import Details from "./Details";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            artists: getData(),
            searchName: '',
            selectedId: '',
            selectedArtist: {
                Comments: "",
                artist_country: "",
                artist_id: '',
                artist_name: "",
                artist_rating: '',
                artist_twitter_url: "",
                updated_time: ""
            },
            modalIsOpen: false,
        };
    }

    handleModal = () => {
        this.setState((prevState) => ({
            modalIsOpen: !prevState.modalIsOpen
        }));
        console.log("handleModal")
    }

    handleSearchChanges = (e) => {
        this.setState({
            searchName: e.target.value,
        });
    };

    onSelect = (id) => {
        this.setState({
            selectedId: id,
            selectedArtist: this.state.artists.find(x => x.artist_id.toString() === id.toString())
        });
    }

    editArtist = (selectedArtist) => {
        const result = this.state.artists.map((item) => {
            if (item.artist_id === selectedArtist.artist_id) {
                return {
                    ...item,
                    artist_name: selectedArtist.artist_name,
                    artist_rating: selectedArtist.artist_rating,
                    Comments: selectedArtist.Comments,
                };
            }
            return item;
        });
        this.setState({
            artists: result,
        });
    }


    handleChange = (e) => {
        const {name} = e.target;
        const value = e.target.value;
        this.setState((prevState) => ({
            selectedArtist: {...prevState.selectedArtist, [name]: value}
        }));
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log("handleSubmit")
        this.editArtist(this.state.selectedArtist);
        editItem(this.state.selectedArtist);
        this.handleModal();
    }

    render() {
        const artists = this.state.artists.filter(artist => {
                return artist.artist_name.toLocaleLowerCase().includes(this.state.searchName.toLocaleLowerCase())
            }
        );

        return (
            <div className="App container p-3">
               <Details
                    selectedArtist={this.state.selectedArtist}
                    handleChange={this.handleChange}
                    handleSubmit={this.handleSubmit}
                    handleModal={this.handleModal}
                />

                <div className="px-0 mb-3">
                    <Search handleSearchChanges={this.handleSearchChanges}/>
                </div>

                <div>
                    <Table
                        artists={artists}
                        onSelect={this.onSelect}
                        handleModal={this.handleModal}
                    />
                </div>
            </div>
        );
    }
}

export default App;
